import { expect } from 'chai';
import {sum, multiply, asyncSum} from '../src/calc';

describe('The calc module', () => {
    context(`#sum`, ()=> {
        it(`should exist`, () => {
            expect(sum).to.be.a('function')
            expect(sum).to.be.instanceOf(Function)
        })

        it(`should sum two numbers`, () => {
            let actual = sum(2,3);
            expect(actual).to.eql(5);
        })

        it(`should sum severals numbers`, () => {
            let actual = sum(2,3,4,5);
            expect(actual).to.eql(14);
        })
    })

    context(`#multiply`, ()=> {
        it(`should exist`, () => {
            expect(multiply).to.be.a('function')
            expect(multiply).to.be.instanceOf(Function)

        })

        it(`should multiply two numbers`, () => {
            let actual = multiply(2,3);
            expect(actual).to.eql([6]);
        })

        it(`should multiply severals numbers`, () => {
            let actual = multiply(2,2,3,4);
            expect(actual).to.eql([4,6,8]);
        })
    })

    context(`#asyncSum`, ()=> {

        it(`should multiply severals numbers`, async () => {
            let actual = await asyncSum(2,3);
            expect(actual).to.eql(5);
        })
    })

})