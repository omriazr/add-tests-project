// import * as P from '@omriazr/promises'

export function sum(...numbers:number[]):number{
    return numbers.reduce((prev,curr) => prev + curr);
}

export function multiply(mult:number,...numbers:number[]):number[]{
    return numbers.map((item) => item * mult);
}

export async function asyncSum(...numbers:number[]):Promise<number>{
    await delay(300);
    return numbers.reduce((prev,curr) => prev + curr);
}

export const delay = (ms: number): Promise<any> =>
    new Promise((resolve) => setTimeout(resolve, ms));